
ardour {
    ["type"]    = "EditorAction",   --  "DSP", "Session", "EditorHook", "EditorAction"
    name        = "Create Song Markers", 
    -- Optional
    author      = "Bruno VERNAY",
    license     = "MIT",
    description = [[Structure the song with markers (or ranges): Verse ; Chorus ; Break ...
        Currently, the song structure is defined in the script
            Could add a dialog box to enter the structure
            Could parse the Session:Metadata description structure if available in Lua
        Might not work if there are Tempo changes!
        The markers are locked and glued to Bars and Beats
    ]]
}

--[[
    With help: https://discourse.ardour.org/t/lua-how-to-create-a-marker-at-specific-bbt/106497

    https://manual.ardour.org/lua-scripting/class_reference/#Timecode:BBT_TIME    BeatTime

    TODO:
      - Manage TempoChanges
      - Add options to not Lock and not Glue

    Not required:
    -- Session:request_stop()
    -- Session:goto_start()
]] 


function factory (unused_params)
    return function ()

        local prefix = "S| "    -- Name prefix for the markers (or ranges)
        local offset = 1        -- Measure 1 is the very first (Not 0!)
        local song = {          -- Adapt to your song { Part name, Length in measures }
            { "Intro",    4 },
            { "A 1",      8 },
            { "B 1",     12 },
            { "A 2",      8 },
            { "B 2",     12 },
            { "Outro",    4 },
        }
        local useRangeInsteadOfMarker = false   -- Can use Ranges instead of Markers

        --- Convert BBT to Position, according to current tempo.
        function bbtToPos(bar, beat, tick)
            local bfc = ARDOUR.DoubleBeatsSamplesConverter(Session:tempo_map(), 0)
            local calcBeat = ((bar - 1) * 4) + (beat - 1) + (tick / 1920)
            return bfc:to(calcBeat)
        end

        --- Create a Marker (or a Range)
        -- @param name Marker name 
        -- @param start Measure number where to start the Range (offset)
        -- @param length Range size in measures
        -- @param useRange if true: creates Ranges ; if false: creates Markers
        -- @return The last measure number (to be used as next offset)
        function create(name, start, length, useRange)
            assert( start > 0, "Measures start at 1!")
            local loc
            local startPos = bbtToPos(start,1,0)
            if useRange then
                loc = Session:locations():add_range(startPos, bbtToPos(start+length,1,0))
            else
                Editor:add_location_mark(startPos)
                loc = Session:locations():mark_at(startPos, startPos)  -- Not reliable!
            end
            loc:set_name(prefix .. name)
            -- Should be locked?
            loc:lock()
            -- Should be Glue to the beat
            return start+length
        end


        -- is there a logger or something?
        print(" * - * Create Song Markers / Ranges  * - * ")

        print(" Clean up ")   -- Remove previous markers and ranges starting with "prefix ..."
        for l in Session:locations():list():iter() do 
            if ( l:is_range_marker() or l:is_mark() ) and string.find (l:name(), "^" .. prefix .. "*") then
                print("Removing previous range: ", l:name())
                Session:locations():remove(l)
            end
        end

        
        local backup_glue_config = Session:cfg():get_glue_new_markers_to_bars_and_beats ()
        Session:cfg():set_glue_new_markers_to_bars_and_beats (true)
        
        print("Song structure:") 
        print("  # ; Start ; Length ; Name") 
        for i,v in ipairs(song) do             
            print(string.format("  %2d ; %4d ; %2d ;\t%s", i, offset, v[2], prefix .. v[1]))
            offset = create(v[1], offset, v[2], useRangeInsteadOfMarker)
        end

        -- Restore config
        Session:cfg():set_glue_new_markers_to_bars_and_beats (backup_glue_config)

    end
end

