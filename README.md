# Ardour-utils

Some *Shell* and *Lua* scripts to deal with  [Ardour](https://ardour.org/) Digital Audio Workstation (DAW).

- Lua scripts
  - `create_markers.lua` Create Song marker to structure the timeline.
- Shell scripts
  - `remove-xrun-markers` can be useful if the session cannot even load. Otherwise, there is a Lua script in Ardour that remove `xruns`.


Note: Lua scripts must be installed in `~/.config/ardour6/scripts/`.


## TODO

- It is possible to launch system commands from within Lua https://github.com/Ardour/ardour/blob/master/share/scripts/_system_exec.lua . That should allow to launch MuseScore for example !
- The current Lua API is for Ardour *v6*.  It is likely that Ardour *v7* will bring some API changes.

