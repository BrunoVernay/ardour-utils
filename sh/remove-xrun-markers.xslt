<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!--

    Remove the XRun location markers from Ardour session file.

    http://stackoverflow.com/questions/5985756/how-to-remove-unwanted-elements-and-attributes-from-xml-file-using-xslt
       
     -->
  <xsl:output indent="yes"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="@*|node()">
    <xsl:copy>
       <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="Location[@name='xrun']"/>

</xsl:stylesheet>

