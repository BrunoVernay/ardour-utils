#!/bin/sh
# Remove the XRun location markers from Ardour session file.

xsltproc -novalid -nonet remove-xrun-markers.xslt "$1"

